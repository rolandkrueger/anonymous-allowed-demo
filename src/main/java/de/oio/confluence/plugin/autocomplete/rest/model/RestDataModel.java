package de.oio.confluence.plugin.autocomplete.rest.model;

import org.codehaus.jackson.annotate.JsonIgnoreProperties;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "data")
@XmlAccessorType(XmlAccessType.NONE)
@JsonIgnoreProperties(ignoreUnknown = true)
public class RestDataModel {

    private boolean someValue;

    protected RestDataModel() {
    }

    public RestDataModel(boolean someValue) {
        this.someValue = someValue;
    }

    @XmlElement(name = "someValue")
    public boolean isSomeValue() {
        return someValue;
    }

    public void setSomeValue(boolean someValue) {
        this.someValue = someValue;
    }
}
