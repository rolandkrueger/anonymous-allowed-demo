package de.oio.confluence.plugin.autocomplete.rest;

import com.atlassian.plugins.rest.common.security.AnonymousAllowed;
import de.oio.confluence.plugin.autocomplete.rest.model.RestDataModel;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

@Path("/configuration")
public class RestPluginConfigurationService {

    @GET
    @AnonymousAllowed
    @Consumes({MediaType.APPLICATION_JSON})
    @Produces({MediaType.APPLICATION_JSON})
    public Response getPluginConfiguration() {
        return Response.ok(new RestDataModel(true)).build();
    }
}
